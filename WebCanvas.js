var canvas = document.getElementById("Canvas");
var ctx = canvas.getContext("2d");

var draw;
var typeIn;
var tool = "brush";
var color = document.getElementById("color-picker").value;
var brush_size = document.getElementById("brush-size").value;
//var btnDownload = document.getElementById('btn-download');
var imageLoader = document.getElementById('imageLoader');

canvas.addEventListener('mousedown',mouseDown);
canvas.addEventListener('mousemove',mouseMove);
canvas.addEventListener('mouseup',mouseUp);

function initial(){
    canvas = document.getElementById("Canvas");
    ctx = canvas.getContext("2d");
    canvas.addEventListener('mousedown',mouseDown);
    canvas.addEventListener('mousemove',mouseMove);
    canvas.addEventListener('mouseup',mouseUp);
    canvas.style.cursor = "url('image/brush.cur'), default";
    ctx.lineWidth = brush_size;
    ctx.strokeStyle = color;
    ctx.lineCap = "round";
}

function mouseDown(e){
    draw=true;

    var o=this;
    this.offsetX=this.offsetLeft;
    this.offsetY=this.offsetTop;

    while(o.offsetParent){
    	o=o.offsetParent;
    	this.offsetX+=o.offsetLeft;
    	this.offsetY+=o.offsetTop;
    }

    switch(tool){
        case "brush":
            ctx.beginPath();
            ctx.moveTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
            break;
        case "eraser":
            ctx.beginPath();
            ctx.moveTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
            break; 
        case "text":
            ctx.fillText(typeIn,e.pageX-this.offsetX,e.pageY-this.offsetY);
            break;
    }
}

function mouseMove(e){
    if (draw){
        switch(tool){
            case "brush":
                ctx.lineTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
                ctx.stroke();
                break;
            case "eraser":
                ctx.lineTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
                ctx.stroke();
                break; 
            case "text":
                break;
        }
    }
}

function mouseUp(e){
    if(draw){
        push();
        draw=false;
    }
}

function pick_color(){
    color = document.getElementById("color-picker").value;
    ctx.strokeStyle = color;
}

function change_size(){
    brush_size = document.getElementById("brush-size").value;
    ctx.lineWidth = brush_size;
}

function set_tool(input){
    tool = input;
    
    switch(tool){
        case "brush":
            ctx.strokeStyle = color;
            ctx.lineWidth = brush_size;
            canvas.style.cursor = "url('image/brush.cur'), default";
            break;
        case "eraser":
            ctx.strokeStyle = "#FFFFFF";
            ctx.lineWidth = brush_size*2;
            canvas.style.cursor = "url('image/eraser.cur'), default";
            break; 
        case "text":
            ctx.fillStyle = "blue";
            var size = document.getElementById("text-size").value + "px";
            var style = document.getElementById("text-style").value;
            ctx.font = size + " " + style;
            console.log(size + " " + style);
            typeIn = document.getElementById("text-content").value;
            canvas.style.cursor = "url('image/Rainbow.cur'), default";
            break;
    }
}

//refresh canvas
function refresh(){
    ctx.clearRect(0,0,canvas.width, canvas.height);
    push();
}

//download as image
//use download in <a>
//ver1. use <a>
/*btnDownload.addEventListener('click', function (e) {
    var dataURL = canvas.toDataURL('image/png');
    button.href = dataURL;
});*/
//ver2. button
function download(){
    image = canvas.toDataURL("image/png", 1.0).replace("image/png", "image/octet-stream");
    var link = document.createElement('a');
    link.download = "canvas.png";
    link.href = image;
    link.click();
}

//upload image to canvas
imageLoader.addEventListener('change', handleImage, false);
function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    push();     
}

//undo redo
var pushArray = new Array();
var doStep = -1;
     
function push() {
    console.log("push");
    doStep++;
    if (doStep < pushArray.length) { pushArray.length = doStep; }
    pushArray.push(document.getElementById('Canvas').toDataURL());
}

function undo() {
    if (doStep > 0) {
        console.log("undo");
        doStep--;
        var canvasPic = new Image();
        canvasPic.src = pushArray[doStep];
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
    else if(doStep == 0){
        doStep--;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
}       

function redo() {
    if (doStep < pushArray.length-1) {
        console.log("redo");
        doStep++;
        var canvasPic = new Image();
        canvasPic.src = pushArray[doStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}                    

//text
/*
function textset(){
    ctx.fillStyle = "blue";
    var size = document.getElementById("text-size").value + "px";
    var style = document.getElementById("text-style").value;
    ctx.font = size + " " + style;
    ctx.fillText("Hello World", (canvas.width / 2), (canvas.height / 2));    
}*/