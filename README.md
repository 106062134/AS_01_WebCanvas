# Software Studio 2018 Spring Assignment 01 Web Canvas

initial():整個畫布開始的設定，預設是筆刷的功能

##Basic control tools (30%)
##Brush and eraser
##Color selector
##Simple menu (brush size)
set_tool() : 可以選擇要用甚麼工具
mouse_Down(), mouse_Move(), mouse_Up() : 偵測滑鼠的動作
    有關取得座標的方式是網路上的，可以準確地得到位子，覺得沒有不好就直接拿來用了
pick_color() : 選擇顏色用的，會把修改過後會把ctx的數值更改
change_size() : 更改筆刷大小

##Text input (10%)
##User can type texts on canvas
##Font menu (typeface and size)
分成三段: 字體，大小，內容
字體:<select> ；大小跟內容是用input
有一個<button>OK， 按下去的時候會set_tool()，把ctx的資料建立起來
這時候點擊畫面，就可以在滑鼠的位子貼上這段文字

##Cursor icon (10%)
##The image should change according to the currently used tool
canvas.style.cursor = "url('image/brush.cur'), default";
只要找好圖片，在set_tool的時候就可以換圖片了

##Refresh button (10%)
##Reset canvas 
ctx.clearRect(0,0,canvas.width, canvas.height);
把整個畫布清空。

##Different brush shapes (15%)
##Circle, rectangle and triangle (5% for each shape)
沒有做，還沒成功

##Un/Re-do button (10%)
toDataURL() : 將畫布轉成一個資料，可以把push進array裡面存起來。
var doStep: 紀錄做個的動作次數。
##Image tool (5%)
##User can upload image and paste it 

讀檔案整個都是看網路上寫的，沒有特別修改

##Download (5%)
##Download current canvas as an image file
用<a>的功能就可以下載
先用toDataURL把畫布的資料存起來，
create一個<a>的標籤，
然後就可以下載。